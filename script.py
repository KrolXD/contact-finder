import os
import re
import requests
import requests.exceptions
from urllib.parse import urlsplit
import googlemaps
import pandas as pd
import signal

def signal_handler(signum, frame):
    raise Exception("Timed out!")

def get_place_info(placeID):
    try:
        response = map_client.place(
            place_id=placeID
        )
        results = response.get('result')
        try:
            web = results["website"]
        except:
            web = "-"
        
        try:
            phone = results["international_phone_number"]
        except:
            phone = "-"

        company = {
            "name": results["name"],
            "website": web,
            "phone number": phone
        }
        return company
    except Exception as e:
        return None
        
def get_emails(url):
    parts = urlsplit(url)
    base_url = "{0.scheme}://{0.netloc}".format(parts)
    path = url[:url.rfind('/')+1] if '/' in parts.path else url

    try:
        response = requests.get(url)
        emails = re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", response.text, re.I)
        return list(dict.fromkeys(emails))
    except (requests.exceptions.MissingSchema, requests.exceptions.ConnectionError):
        return " - "

API_KEY = '--YOUR GOOGLE MAPS API KEY--'
map_client = googlemaps.Client(API_KEY)

address = input("Enter adress: ") 
geocode = map_client.geocode(address=address)
(lat, lng) = map(geocode[0]['geometry']['location'].get, ('lat', 'lng'))


search_string = input("Enter keyword: ") 
distance = input("Enter range in meters: ") 
business_list = []

response = map_client.places_nearby(
    location=(lat, lng),
    keyword=search_string,
    radius=distance
)
signal.signal(signal.SIGALRM, signal_handler)
signal.alarm(10)
for res in response.get('results'):
    emailslist = []
    placeID = res['place_id']
    placeInfo = get_place_info(placeID)

    if(placeInfo.get("website") != '-'):
        try:
            emailslist = get_emails(placeInfo.get("website"))
        except Exception:
            emailslist = " - "
    placeInfo["Emails"] = emailslist
    business_list.append(placeInfo)
    
next_page_token = response.get('next_page_token')

df = pd.DataFrame(business_list)
print(df)
df.to_csv('test.csv')
